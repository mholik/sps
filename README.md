# sps




## Name
A system for transmitting 4K video footage with low latency over short distances

## Description
Using the ASI183MC Pro camera and the accompanying SDK, two application were built: SPS 1 and 2.  
SPS 1 uses GStreamer to form a video stream which can be accessed over the local network using a program like VLC.  
SPS 2 on the other hand doesn't use any application level protocols. It packages the image data into UDP datagrams, which the provided client app which receives, decodes and displays.



# Setting up the environment

ZWO provides SDKs for both Linux and Windows. Both were tested and used, so choose which ever one you prefer.

The ready made software for controlling the camera, as well as the developer SDK can be found [here](https://astronomy-imaging-camera.com/software-drivers)





## Linux:

### Installing ASIStudio
1) download [ASIStudio.run](https://www.zwoastro.com/downloads/linux)
2) ```chmod +x ASIStudio.run```
3) ```./ASIStudio.run```
the installer automatically handles everything

using ASIStudio:
- careful of disk space: 10s of 5K avi produces 11Gb of data!
- in setting, tick boxes are a bit broken:
	- when they're unselected you see them, however if you select them a tick doesn't appear, instead the box just disappears (but its still there).
	- So, if you want to turn of lets say "Save as tiff", just click left of the label
	- note: TIFF is only available in RAW16, while RAW8 can only be PNG
- "RAW data" changes the format from "No Debay PNG" to "Color PNG"
	- you can see the format in the bottom right of the menu
	- This setting also overrides the Debayering button in the video pane
- in the '...' menu, under control, you can set the USB traffic speed (auto is default), "High Speed" mode, Hardware binning and more
- eaf == Electronic Automatic Focuser, efw == Electronic Filters Wheels




### Installing the SDK and running the default examples
1) install dependencies:
```
sudo apt-get install -y build-essential pkg-config libssl-dev git net-tools libncurses-dev
sudo apt install libopencv-dev python3-opencv
```
note: if you want platform=x86, install necessary m32 libs with: ```sudo apt-get install gcc-multilib```  

2) download the [ASI Camera SDK](https://www.zwoastro.com/downloads/developers)  
3) unzip with ```tar '-xvjf ASI_linux_mac_SDK_V1.28.tar.bz2'```
4) ```cd ASI_linux_mac_SDK_V1.28```

5) ```cd /etc/ld.so.conf.d/```
6) ```sudo touch libASICamera2.conf```
7) ```sudo gedit libASICamera2.conf```, then add the path to the ~/ASI_linux_mac_SDK_V1.28/lib/x64 folder
e.g., if you unpacked the SDK to /home/user/zwo, then add "/home/user/zwo/ASI_linux_mac_SDK_V1.22/lib/x64", then save and exit
8) ```sudo ldconfig```

9) ```cd ~/ASI_linux_mac_SDK_V1.22/demo/bin```
10) ```mkdir x64```
11) replace 1 line in main_SDK2_snap.cpp and main_SDK2_snap.cpp_video.cpp:
	from: "cvSet(pRgb, CV_RGB(180, 180, 180));"
	to: "cvSet(pRgb, cvScalar(180, 180, 180));"
12) ```cd /demo```
13) ```make platform=x64```
3 applications are created in /demo/bin/x64
14) ```cd /demo/bin/x64```
15) ```chmod +x main_SDK2_video_mac test_gui2_snap test_gui2_video```
note: apps in demo/bin/x86 are prebuilt, so it has nothing to do with new x64 builds
16) connect the camera
17) ```sudo ./test_gui2_video```
from there follow the onscreen instructions


troubleshooting:
- depending on the version of OpenCV that you install, the makefile may need to be edited for the header to be visible. For instance, if you install opencv4, you will need to modify the OPENCV variable:
from: OPENCV = -lopencv_core -lopencv_highgui -lopencv_imgproc#$(shell pkg-config --cflags opencv) $(shell pkg-config --libs opencv) -I/usr/include/opencv4/opencv/
to:   OPENCV = -lopencv_core -lopencv_highgui -lopencv_imgproc $(shell pkg-config --cflags opencv4) $(shell pkg-config --libs opencv4) -I/usr/include/opencv4/opencv2/
 - If you see errors indicating usb lib is missing, make sure the '~/ASI_linux_mac_SDK_V1.28/lib/x64/libASICamera2.so' is named just like that. There shouldn't be anything appended to the end, like ".1.27"
 - If you're getting errors for the ASI DLLs, make sure you did the steps 4-7 in the correct path
 - Sometimes the build files get corrupted, but that can be fixed by running ```make clean``` in /demo



## Windows:

### Running demo.exe
1) Download and install the [ASI Cameras ZWO Windows driver](https://www.zwoastro.com/downloads/windows) ([direct download](https://dl.zwoastro.com/software?app=AsiCameraDriver&region=Overseas))
2) Download and unzip the [ASI Camera SDK](https://www.zwoastro.com/downloads/developers)
3) add these paths to system PATH
	- SDK\lib\x86 				#(ASICamera2.dll)
	- SDK\demo\opencv2\lib 		#(opencv_imgproc247.lib)
	- SDK\demo\opencv2\bin		#(opencv_imgproc247.dll)
	or, alternatively, place the DDLs to the directory where you want to run the demos
4) run demo.exe


### Build the demo2.exe
1) download and install [Visual Studio 19 Community](https://visualstudio.microsoft.com/vs/older-downloads/) and add the C++ Development components
2) Visual Studio Installer > Modify > Individual Components:  
	**make sure the version of components you choose matches the version in the Project Properties (e.g. v142)**
	- MSVC v142 - VS 2019 C++ x64/86 build tools (latest)
	- C++ MFC for latest v142 build tools (x86 & x64)
	- C++ ATL for latest v142 build tools (x86 & x64)
3) Download and unzip the [ASI Camera SDK](https://www.zwoastro.com/downloads/developers)
4) Open demo2.sln

note: if it says some files exist from demo.sln and asks to replace them click "No"
5) Project Properties > Linker > Input > Additional Dependancies > Edit (little arrow on the right)
modify first line to: ../../lib/x86/ASICamera2.lib
6) Build the project (Right click on the project in the Solution Explorer on the right of the screen, or use the "Local Windows Debugger" button on the top)
7) demo2.exe is created in /Debug
note: demo2.exe in ../ is a prebuilt app, so it has nothing to do with new builds


notes:
 - if you accidentally add "SDK\lib\x64" to the path and run demo.exe, you will get: "The application was unable to start correctly (0xc000007b)"
 - the demo source code appears to be written for VS2010 SP1, so a migration to 2019 is required and expected
 - the original ReadMe.txt notes file has an encoding problem so the characters don't display properly. To fix, open the file in Notepad++ or rename it to ReadMe.html and open using a browser



# SPS 1 and 2

Once you configure your environment of choice, we can move on to building and running the SPS apps.
All the examples require additional codecs, Gstreamer libraries, Boost and the Tbb library, so we'll demonstrate installing those




## Linux
1) install additional codecs, Gstreamer libraries, Boost and the Tbb library using apt-get
```
sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev h264enc libx264-dev
sudo apt-get install libavcodec-extra-52 libavdevice-extra-52 libavfilter-extra-0 libavformat-extra-52 libavutil-extra-49 libpostproc-extra-51 libswscale-extra-0
sudo apt-get install libgstreamer1.0-dev libgstrtspserver-1.0-dev gstreamer1.0-libav gstreamer1.0-plugins-ugly libgstreamer-plugins-base1.0-dev gstreamer1.0-rtsp
sudo apt-get install libboost-all-dev
sudo apt install libtbb-dev
```

2) replace the default Makefile with the one provided in this repo. It adds the headers and libraries for Boost, Gst and Tbb




## Windows, using VS19


1) Install vcpkg by following the [official instructions](https://vcpkg.io/en/getting-started.html)
2) Install the required libraries using:
```
vcpkg install boost #note: this took 28 minutes on the author's machine
vcpkg install opencv #note: this took 10 minutes on the author's machine
vcpkg install tbb
vcpkg install ffmpeg[core,avcodec,avdevice,avfilter,avformat,ffmpeg,ffplay,ffprobe,amf,opencl,x264,x265,swscale,nonfree] (Total install time: 15 min)
```
3) Run VS19 and create a new blank project
4) vcpkg installs x86 dependancies, so we need to choose x86 as the target in VS19. This can be done using a drop-down menu at the top, next to the "Local Windows Debugger" button
5) Copy and paste the code of the desired version into the new project
6) Build the project






# Android App

Used equipment:
- ASI183MC Pro camera
- ASIAIR PRO controller
- SD containing the configuration and a USB stick for saving the images from the ASIARI PRO controller
- USB 3.0 Type-A male to Type-B male
- 12V DC 2.0A power supply (Tuning Fork DC Plug 5.5*2.5mm DC Molding Cable)

1) download and install ASIARI Android app (from WZO)
2) Plug in a SD into the camera and the USB into one of the 2.0 port on ASIAIR pro
3) Connect the camera to the ASIARI using the USB 3.0 port and connect the power supply
4) Turn on ASIARI and wait around 10 seconds (it will make a beep when it's ready)
5) Connect to ASIAIR's Wi-Fi using your phone
	- the SSID and password can be found on the back of the device; e.g. "SSID: ASIAIR_e45a4326, PASS:12345678"
6) Turn on the ASIAIR app, select the desired camera and start
7) Change the mode to Video to see a live preview
	- AVI is raw (saves on SD card), MP4 isn't (saves on phone)

Using the app:
 - top right (Preview): change mode
 - top: settings shortcuts
 - BinX settings: 2x the bin means /2 the resolution but 2* the brightness
