
The randomGeneration implementation is a modification of the [gst-rtsp appsrc](https://github.com/GStreamer/gst-rtsp-server/blob/master/examples/test-appsrc.c) example.  

The cameraInput implementation incorporates the randomGeneration code into the ZWO camera code, allowing the camera image to be streamed.
