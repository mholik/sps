The second SPS implementation.  

A separate server and client program are used and both need to run for the data to start flowing.  

In case one of them crashes it is recommended to restart the other.
